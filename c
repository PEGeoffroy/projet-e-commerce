[33mcommit f8b7f51268dba4612e69bc8977f02ab39bf50f6d[m[33m ([m[1;36mHEAD -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m, [m[1;31mmain/master[m[33m)[m
Merge: dddd036 58c9d4d
Author: AOUGABI <amel.aougabi@gmail.com>
Date:   Fri Jul 27 15:09:15 2018 +0000

    Merge branch 'master' into 'master'
    
    add files:diagrams
    
    See merge request groupe-projet-e-commerce/projet-e-commerce!4

[33mcommit 58c9d4d3c6325527c2de5dfc559f65ff65221053[m
Author: AOUGABI <amel.aougabi@gmail.com>
Date:   Fri Jul 27 17:07:50 2018 +0200

    add files:diagrams

[33mcommit dddd03682786a57b2c7dcda90223006c492b4677[m
Merge: a4ffc4f 5bab6c3
Author: Pierre-Élie Geoffroy <p-e.geoffroy@hotmail.fr>
Date:   Fri Jul 27 13:27:06 2018 +0000

    Merge branch 'master' into 'master'
    
    Add history for command and add icon for edit information
    
    See merge request groupe-projet-e-commerce/projet-e-commerce!3

[33mcommit 5bab6c3624e41fc823c3c7ac266bada45afea593[m
Author: Pierre-Élie Geoffroy <p-e.geoffroy@hotmail.fr>
Date:   Fri Jul 27 15:26:03 2018 +0200

    Add history for command and add icon for edit information

[33mcommit a4ffc4fc6850ee3943303814aa83d9cd705340f9[m
Merge: 1e5607b 69c0561
Author: Pierre-Élie Geoffroy <p-e.geoffroy@hotmail.fr>
Date:   Fri Jul 27 12:47:24 2018 +0000

    Merge branch 'master' into 'master'
    
    add modification diagram
    
    See merge request groupe-projet-e-commerce/projet-e-commerce!2

[33mcommit 69c0561d9be6a7e11b0e99d7e12c4f5019c8b1ed[m
Author: Pierre-Élie Geoffroy <p-e.geoffroy@hotmail.fr>
Date:   Fri Jul 27 14:44:45 2018 +0200

    add modification diagram

[33mcommit 1e5607bce4d940ff3f0dcfffd64ab17cb5599242[m
Merge: f3aa44f 0561bd9
Author: Pierre-Élie Geoffroy <p-e.geoffroy@hotmail.fr>
Date:   Thu Jul 26 13:58:03 2018 +0000

    Merge branch 'master' into 'master'
    
    Add new file diagram and update readme
    
    See merge request groupe-projet-e-commerce/projet-e-commerce!1

[33mcommit 0561bd914e2502a04f0a1fe69b6268ae1e8a843d[m
Author: Pierre-Élie Geoffroy <p-e.geoffroy@hotmail.fr>
Date:   Thu Jul 26 15:57:23 2018 +0200

    Add new file diagram and update readme

[33mcommit f3aa44f3a0120ceec7563dd0b470939a6075c399[m
Author: Pierre-Élie Geoffroy <p-e.geoffroy@hotmail.fr>
Date:   Thu Jul 26 12:39:03 2018 +0000

    Ajout de README.md
