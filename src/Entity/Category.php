<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="array")
     */
    private $product;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="relation", orphanRemoval=true)
     */
    private $products_id;

    public function __construct()
    {
        $this->products_id = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProduct(): ?array
    {
        return $this->product;
    }

    public function setProduct(array $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProductsId(): Collection
    {
        return $this->products_id;
    }

    public function addProductsId(Product $productsId): self
    {
        if (!$this->products_id->contains($productsId)) {
            $this->products_id[] = $productsId;
            $productsId->setRelation($this);
        }

        return $this;
    }

    public function removeProductsId(Product $productsId): self
    {
        if ($this->products_id->contains($productsId)) {
            $this->products_id->removeElement($productsId);
            // set the owning side to null (unless already changed)
            if ($productsId->getRelation() === $this) {
                $productsId->setRelation(null);
            }
        }

        return $this;
    }
}
