<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class CartLineController extends Controller
{
    /**
     * @Route("/cart/line", name="cart_line")
     */
    public function index()
    {
        return $this->render('cart_line/index.html.twig', [
            'controller_name' => 'CartLineController',
        ]);
    }
}
